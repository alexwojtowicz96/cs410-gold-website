<?php

$fname_error = "";
$lname_error = "";
$email_error = "";
$msg_error = "";

$fname = "";
$lname = "";
$email = "";
$msg = "";

$success = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (empty($_POST["fname"])) {
		$fname_error = "*'First Name' is a required field";
	}
	else {
		$fname = test_input($_POST["fname"]);
		if (!preg_match("/^[a-zA-Z]*$/", $fname)) {
			$fname_error = "*Only letters are allowed in this field";
			$fname = "";
		}
	}

	if (empty($_POST["lname"])) {
		$lname_error = "*'Last Name' is a required field";
	}
	else {
		$lname = test_input($_POST["lname"]);
		if (!preg_match("/^[a-zA-Z]*$/", $lname)) {
			$lname_error = "*Only letters are allowed in this field";
			$lname = "";
		}
	}

	if (empty($_POST["email"])) {
		$email_error = "*'Email' is a required field";
	}
	else {
		$email = test_input($_POST["email"]);
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$email_error = "*Invalid email format";
			$email = "";
		}
	}

	if (empty($_POST["msg"])) {
		$msg_error = "*'Message' is a required field";
	}

	// Validation is complete, now submit the form if no errors found...
	if ($fname_error == "" and $lname_error == "" and $email_error == "" and $msg_error == "") {
		$msg_body = "You have recieved a new message regarding Tutor Dash!\n\n";
		unset($_POST["submit"]);
		foreach ($_POST as $key => $value) {
			$msg_body .= "$key: $value\n";
		}

		$to = "awojt001@odu.edu";
		$subject = "Tutor Dash Message";
		if (mail($to, $subject, $msg_body)) {
			$success = "Message sent. Thank you for contacting us!";
			$fname = "";
			$lname = "";
			$email = "";
			$msg = "";
		}
	}
}

function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);

	return $data;
}

?>