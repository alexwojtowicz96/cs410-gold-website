<nav class="MainNav">
	<a href="/~410gold/index.php">
		<img class="logo" src="/~410gold/images/icons_logos/head_logo.png" >
	</a>
	<a class="MenuItems" href="/~410gold/references.php">References</a>
	<a class="MenuItems" href="/~410gold/user_stories.php">User Stories</a>
	<nav class="dropdown">
		<button class="dropButton">Deliverables
			<i class="fa fa-caret-down"></i>
		</button>
		<nav class="dropdown-content">
			<a href="/~410gold/deliverables/competition.php">Competition Matrix</a>
			<a href="/~410gold/deliverables/process_flows.php">Current Process Flows</a>
			<a href="/~410gold/deliverables/solution_flow.php">Solution Process Flow</a>
			<a href="/~410gold/deliverables/mfcd.php">Major Functional Component Diagram</a>
			<a href="/~410gold/deliverables/wbs.php">Work Breakdown Structure</a>
			<a href="/~410gold/deliverables/algorithms.php">Algorithms</a>
			<a href="/~410gold/deliverables/risks.php">Risk Matrix</a>
		</nav>
	</nav>
	<nav class="dropdown">
		<button class="dropButton">Presentations
			<i class="fa fa-caret-down"></i>
		</button>
		<nav class="dropdown-content">
			<a href="/~410gold/presentations/individual.php">Idea</a>
			<a href="/~410gold/presentations/feasibility.php">Feasibility</a>
			<a href="/~410gold/presentations/feasibility_updated.php">Updated Feasibility</a>
			<a href="/~410gold/presentations/design.php">Design</a>
			<a href="/~410gold/presentations/design_updated.php">Updated Design</a>
			<a href="/~410gold/presentations/prototype.php">Prototype</a>
		</nav>
	</nav>
	<a class="MenuItems" href="/~410gold/team.php">Team</a>
	<nav class="dropdown">
		<button class="dropButton">About
			<i class="fa fa-caret-down"></i>
		</button>
		<nav class="dropdown-content">
			<a href="/~410gold/about/about.php">What is Tutor Dash?</a>
			<a href="/~410gold/about/the_problem.php">The Problem</a>
			<a href="/~410gold/about/the_solution.php">The Solution</a>
		</nav>
	</nav>
	<a class="MenuItems" href="/~410gold/index.php">Home</a>
</nav>
