<!DOCTYPE html>
<html>
	<title>Tutor Dash - Solution</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/about/the_solution.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="boxBorders">
			<h1>What is our Solution?</h1>
			<hr class="headerUnderline">
		</div>
		<div class="boxBorders soluStatement">
			<h2>Solution Statement</h2>
			<hr class="headerUnderline">
			<p class="quote">"Tutor Dash is an application-based tutoring service that unifies university students who are interested in tutoring with other university students who desire course-specific tutoring, in real-time. Tutor Dash provides an extension of any university&apos;s current tutoring services so that students can receive help for all courses 24/7."</p><br>
		</div>
		<div class="floatDivs">
			<img class="art" src="../images/icons_logos/phone_screen.png">
			<div class="boxBorders solChars">
				<h2>Solution Characteristics</h2>
				<hr class="headerUnderline">
				<ul>
					<li>Tutor Dash allows qualified students to offer their <mark>tutoring</mark> services to interested students <mark>in any class at their university.</mark></li>
					<li>Tutors can make themselves <mark>available at any time</mark> of the day that is convenient for them.</li>
					<li>Tutor Dash <mark>consolidates tutors</mark>. Whether they work for the university at a tutoring center, or they are private, you can find them using our Tutor Dash.
					<li>Tutoring will be <mark>available in-person or online 24/7.</mark></li>
					<li><mark>Information</mark> regarding the student&apos;s university&apos;s tutoring centers can be <mark>maintained more conveniently and in real-time</mark>. University-affiliated tutors can use the app to &quot;check-in/out&quot; of the tutoring centers, so that tutees can see if the course they desire tutoring for has a tutor available.</li>
					<li><mark>Every tutor is required to provide verification</mark> indicating that they are qualified to tutor in any particular course that they wish to provide tutoring services for.</li>
					<li><mark>Only university students can use Tutor Dash.</mark> Every user needs to have a university-affiliated email address upon creating an account.</li>
					<li>Tutor Dash will <mark>provide a mechanism for payments</mark> to take place within the application.</li>
					<li>Tutor Dash supports the <mark>capability for tutors to provide their location</mark> and relative distance from tutees.</li>
				</ul>
			</div>
		</div>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>