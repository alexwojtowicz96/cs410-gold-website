<!DOCTYPE html>
<html>
	<title>Tutor Dash - About</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/about/about.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="Intro">
			<h1>What is Tutor Dash?</h1>
			<hr class="headerUnderline">
			<p>We are making tutoring more centralized and convenient for university students. Tutor Dash is a mobile application that merges the current tutoring resources at several universities with the current process flows of students seeking tutoring and students seeking advertisement for tutoring into one convenient tool. With such a platform, finding a tutor and being a tutor has never been this easy!</p>
			<img src="../images/icons_logos/graphic.png" width="800px" height="400px" align="middle">
		</div>
		<div class="conceptBox">
			<h2>The Concept</h2>
			<hr class="headerUnderline">
			<br>
			<div class="pChars2">
				<ul>
					<li>There are various instances where students need academic assistance.</li>
					<li>Universities provide tutoring services, but sometimes they don&apos;t suffice.</li>
					<li>Many students attending university qualify to be tutors, but often, they aren&apos;t employed by the university.</li>
					<li>Tutor Dash combines all of these factors into one solution. By allowing students to become tutors, this makes tutoring more appealing, convenient, and widespread for all parties involved!</li>
				</ul>
			</div>
		</div>
		<div class="illus">
			<img src="../images/icons_logos/about_graphic.png" width=100% height=100%>
		</div>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>