<!DOCTYPE html>
<html>
	<title>Tutor Dash - Problem</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/about/the_problem.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="boxBorders">
			<h1>What is the Problem?</h1>
			<hr class="headerUnderline">
		</div>
		<br>
		<div class="boxBorders pStmnt">
			<h2>Problem Statement</h2>
			<hr class="headerUnderline">
			<p class="quote">"Tutoring services available to university students are limited in scope, do not provide flexibility, and lack a centralized platform for promotion. Students willing to provide their own tutoring services lack a tool to promote those services."</p><br>
		</div>
		<br>
		<div class="floatDivs">
			<div class="boxBorders pChars">
				<h2>Problem Characteristics</h2>
				<hr class="headerUnderline">
				<ul>
					<li><mark>Very limited scope</mark>. For example, ODU currently offers tutoring for 132 courses. With over 2400 courses available, this means <mark>ODU&#39;s tutoring services only cover about 5% of all courses.</mark> Over half of these available courses are under 300 level.</li>
					<li><mark>Tutoring centers aren't always available</mark>. For example, at ODU, campus tutoring services are only available an average of 40 hrs/week. With 168 hours in a week, <mark>only about 25% of all available time is being utilized.</mark></li>
					<li><mark>University tutoring services are time-restricted to very narrow windows</mark> sometimes on only one or two days of the week. Some tutoring is exclusively by-appointment.</li>
					<li>Studies suggest that more university students study at night than during the day. Let ODU be an example. The <mark>majority of ODU&#39;s tutoring services are exclusively offered during the daytime.</mark>
					<li><mark>Information regarding several universitys&apos; tutoring services is spread over various places</mark> on their websites which leads to multiple instances of misinformation.</li>
					<li><mark>Current information regarding when and where certain course-specific tutoring sessions is not always reliable</mark>, meaning that it is not uncommon for students who arrive at a tutoring center to discover no tutors are available for them.</li>
					<li><mark>Students who wish to seek tutoring outside of the university cannot always trust tutors</mark> since they are not verified by the university.</li>
					<li>Qualified <mark>students</mark> looking to become private tutors on-campus <mark>do not have an adequate plaform to advertise their services.</mark></li>
				</ul>
			</div>
			<div class="boxBorders stmnts">
				<img src="../images/icons_logos/studentResources.jpeg">
				<p>University tutoring is decentralized, there is a shortage of tutors and there is a surplus of underutilized student resources</p>
				<img src="../images/icons_logos/studentAdver.jpeg">
				<p>Students looking to advertise their own tutoring resources have difficulties finding the best platform to do so.</p>
				<img src="../images/icons_logos/studentStruggle.png">
				<p>Students have difficulties finding, both university and private, tutors that best suit their specific needs.</p>
			</div>
		</div>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>