<!DOCTYPE html>
<html>
	<title>Tutor Dash - Team</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="css/team.css">
		<link rel="stylesheet" type="text/css" href="css/nav.css">
		<link rel="stylesheet" type="text/css" href="css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox">
			<h1>Meet Team Gold!</h1>
			<hr class="headerUnderline">
			<br>
			<div class="Blurb">
				<h3>Team Gold is a software engineering group from Old Dominion University for CS410 Spring 2019. Team Gold&apos;s aim is to make tutoring more centralized across university campuses by connecting students who understand the material with students who don&apos;t in real-time 24/7.</h3>
				<br>
			</div>
		</div>
		<div class="MemSection">
			<div class="TeamMemContainer">
				<img src="images/team/AlexWojtowicz.jpg"> 
				<h2>Alex Wojtowicz</h2>
				<hr class="headerUnderline">
				<h4>Project Manager<br>DB/Algorithms Developer</h4>
				<hr class="headerUnderline">
				<p>Alex is a senior majoring in computer science at Old Dominion University. In addition to writing code, he also enjoys music production and graphic design. Alex is currently seeking a summer internship with a focus in software development.
				</p>
			</div>
			<div class="TeamMemContainer">
				<img src="images/team/BrandonCampbell.jpg"> 
				<h2>Brandon Campbell</h2>
				<hr class="headerUnderline">
				<h4>DB Manager<br>Back-End Developer</h4>
				<hr class="headerUnderline">
				<p>Brandon is a senior majoring in computer science at Old Dominion University. Before attending ODU, Brandon served six years in the U.S. Navy as an electronics technician. In his free time, he likes to spend time with his wife and enjoy the restaurant and craft beer scene in Hampton Roads. Brandon hopes to pursue a career as an Android developer or as a data engineer.
				</p>
			</div>
			<div class="TeamMemContainer">
				<img src="images/team/DuncanHolterhaus.png"> 
				<h2>Duncan Holterhaus</h2>
				<hr class="headerUnderline">
				<h4>Back-End Developer<br>Algorithms Developer</h4>
				<hr class="headerUnderline">
				<p>Duncan is a senior studying Computer Science at Old Dominion University. In his spare time, he plays music, video games, board games with friends.
				</p>
			</div>
			<div class="TeamMemContainer">
				<img src="images/team/DwightOwings.png"> 
				<h2>Dwight Owings</h2>
				<hr class="headerUnderline">
				<h4>Tester<br>Quality Assurance</h4>
				<hr class="headerUnderline">
				<p>Dwight Owings was born in Richmond, Virginia and moved to Chesapeake, Virginia in August 2003, only weeks before Hurricane Isabel hit.  Dwight is a computer science major at Old Dominion University who plans to graduate in 2020.
				</p>
			</div>
			<div class="TeamMemContainer">
				<img src="images/team/jamauni.jpeg"> 
				<h2>Jamauni Taylor</h2>
				<hr class="headerUnderline">
				<h4>Webmaster<br>UI-UX Developer</h4>
				<hr class="headerUnderline">
				<p>Jamauni is a Senior Computer Science undergrad at Old Dominion Univerity. As a computer scientist, Jamauni spends a lot of his time learning the ends and out of website/webserver best practices; Jamauni aims to help create or re-establish thriving communities. During his freetime, Jamauni is a fisherman and/or thinking about ways to make life better.</p>
			</div>
			<div class="TeamMemContainer">
				<img src="images/team/JohnHessefort.jpg"> 
				<h2>John Hessefort</h2>
				<hr class="headerUnderline">
				<h4>UI-UX Developer/Tester<br>Domain Expert</h4>
				<hr class="headerUnderline">
				<p>John is a tutor at the Math and Science Resource Center on campus. He started the official computer science tutoring program, and finds great pleasure in being able to regularly interact with others and help them with their education. He was recently accepted into the Honors College and Linked BSCS/MS degree program, and has since begun research regarding computational biology.</p>
			</div>
		</div>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("modules/footer.php");
		?>
	</body>
</html>
