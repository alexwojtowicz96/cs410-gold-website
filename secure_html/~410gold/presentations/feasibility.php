<!DOCTYPE html>
<html>
	<title>Tutor Dash - Feasibility</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/presentations/feasibility.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox2">
			<h1>Feasibility Presentation</h1>
			<hr class="headerUnderline"><br>
		</div>
		<br>
		<div class="backgroundBox">
			<br><br>
			<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQq3oeuf_GlCjZyPBX7AP7N5qz-iozWsl6J_9XyJ6VCrLfHLWUmDz95PnLqeDIgLhJWVS7L3lbxrsDG/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
			<br><br>
			<a href="pdfs/feasibility1.pdf"><h3>Download the PDF</h3></a>
			<br><br>
		</div>
		<br><br>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>