<!DOCTYPE html>
<html>
	<title>Tutor Dash - Prototype</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/presentations/prototype.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox2">
			<h1>Prototype Presentation</h1>
			<hr class="headerUnderline"><br>
		</div>
		<br>
		<div class="backgroundBox">
			<br><br>
			<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQtPZKGZduBoWwlFq0J-vXIqUq1afKE5Tlt0kqv18RoCdVwOYildtlq6nJCHi4GyUJupD5-F-hSSpsm/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
			<br><br>
			<a href="pdfs/prototype.pdf"><h3>Download the PDF</h3></a>
			<br><br>
		</div>
		<br><br>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>