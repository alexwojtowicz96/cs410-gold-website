<!DOCTYPE html>
<html>
	<title>Tutor Dash - Updated Feasibility</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/presentations/feasibility_updated.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox2">
			<h1>Updated Feasibility</h1>
			<hr class="headerUnderline"><br>
		</div>
		<br>
		<div class="backgroundBox">
			<br><br>
			<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR43sXHHOhpBr5mVK2JwPW7OkGtm-9e03PP_wem6WoRXYbMASPOdIYTB4Vm7pggIL1-aTBW1W76q1s8/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
			<br><br>
			<a href="pdfs/feasibility2.pdf"><h3>Download the PDF</h3></a>
			<br><br>
		</div>
		<br><br>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>