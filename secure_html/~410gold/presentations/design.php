<!DOCTYPE html>
<html>
	<title>Tutor Dash - Design</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/presentations/design.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox2">
			<h1>Design Presentation</h1>
			<hr class="headerUnderline"><br>
		</div>
		<br>
		<div class="backgroundBox">
			<br><br>
			<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTD985HIUd9nJ76p9Ut34wH6mxzAM3Quq8FtKhl7NkY0PpxyRHPdhlHa5x4sF8GbU1yOOAhl5EQt6AB/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true">
			</iframe>
			<br><br>
			<a href="pdfs/design.pdf"><h3>Download the PDF</h3></a>
			<br><br>
		</div>
		<br><br>
		<div class="backgroundBox2">
			<h1>Design Presentation Handout</h1>
			<hr class="headerUnderline"><br>
		</div>
		<br>
		<div class="backgroundBox">
			<br><br>
			<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTxTMZ7ZylWXLylxyHJgAwAxhKVC02lXZM2sDLy6cC3GAJ7ZZQGGxPQ7s1ZkGwbQ_z-BgGFslKclrX1/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
			<br><br>
			<a href="pdfs/design_handout.pdf"><h3>Download the PDF</h3></a>
			<br><br>
		</div>
		<br><br>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>