<!DOCTYPE html>
<html>
    <title>Contact Us</title>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="images/icons_logos/favicon2.png"/>
        <link rel="stylesheet" type="text/css" href="css/contact.css">
        <link rel="stylesheet" type="text/css" href="css/nav.css">
        <link rel="stylesheet" type="text/css" href="css/footer_index.css">
    </head>
    <body>
        <?php
            // fetches contents from navi.php file in modules folder
            echo file_get_contents("modules/navi.php");
            include('modules/form_process.php');
        ?>
        <div class="backgroundBox">
            <h1>Contact Us</h1>
            <hr class="headerUnderline"><br>
        </div>
        <div id="form-main">
            <div id="form-div">
                <form class="form" action="<?= $_SERVER['PHP_SELF']; ?>"  id="form1" method="post">
                    <p class="name">
                        <input name="fname" type="text" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="First Name*" id="name" value="<?= $fname ?>">
                        <span class="error"><?= $fname_error ?></span>
                    </p>
                    <p class="name">
                        <input name="lname" type="text" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="Last Name*" id="name" value="<?= $lname ?>">
                        <span class="error"><?= $lname_error ?></span>
                    </p>
                    <p class="email">
                        <input name="email" type="text" class="validate[required,custom[email]] feedback-input" id="email" placeholder="Email*" value="<?= $email ?>">
                        <span class="error"><?= $email_error ?></span>
                    </p>
                    <p class="text">
                        <textarea name="msg" type="text" class="validate[required,length[6,300]] feedback-input" id="comment" placeholder="Message*" value="<?= $msg ?>"></textarea>
                        <span class="error"><?= $msg_error ?></span>
                    </p>
                    <div class="submit">
                        <input type="submit" value="SEND" id="button-blue">
                        <div class="ease"></div>
                    </div>
                    <div class="success"><?= $success ?></div>
                </form>
            </div>
        </div>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
        <?php
            echo file_get_contents("modules/footer.php");
        ?>
    </body>
</html>