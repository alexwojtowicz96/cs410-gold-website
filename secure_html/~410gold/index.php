<!DOCTYPE html>
<html>
	<title>Tutor Dash - Home</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" type="text/css" href="css/nav.css">
		<link rel="stylesheet" type="text/css" href="css/footer_index.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("modules/navi.php");
		?>
		<div class="backgroundBox">
			<br><br><br>
			<div class="overlay-image shadowed1"><a href="about/about.php">
 				<img class="image" src="images/icons_logos/back-layer.png">
 				<div class="normal">
  					<div class="text"></div>
 			</div>
 			<div class="hover shadowed2">
  				<img class="image" src="images/icons_logos/front-layer.png">
 				<div class="text">Learn More</div>
 			</div>
			</a></div>
			<br><br>
		</div>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("modules/footer.php");
		?>
	</body>
</div>
</html>