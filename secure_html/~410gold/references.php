<!DOCTYPE html>
<html>
	<title>Tutor Dash - References</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="css/references.css">
		<link rel="stylesheet" type="text/css" href="css/nav.css">
		<link rel="stylesheet" type="text/css" href="css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox">
			<h1>References</h1>
			<hr class="headerUnderline">
		</div>
		<br>
		<div class="backgroundBox2">
		<h2>University Tutoring Resources</h2>
			<hr class="divider gold"><br>
			<div class="References">
				<ol>
					<li>&quot;Academics.&quot; <i>Old Dominion University</i>, 8 Jan. 2019. URL: <a href="http://www.odu.edu/academics"><mark class="gold">www.odu.edu/academics</mark></a>.</li>
					<li>&quot;Campus Tutoring.&quot; <i>Old Dominion University</i>, 19 Jan. 2019. URL: <a href="http://www.odu.edu/success/academic/tutoring#tab125=0"><mark class="gold">www.odu.edu/success/academic/tutoring#tab125=0</mark></a>.</li>
					<li>&quot;Course-Specific Tutoring.&quot; <i>Old Dominion University</i>, 19 Jan. 2019. URL: <a href="http://www.odu.edu/success/academic/tutoring/course-specific"><mark class="gold"="gold">www.odu.edu/success/academic/tutoring/course-specific</mark></a>.</li>
					<li>&quot;Courses of Instruction.&quot; <i>Old Dominion University</i>, Feb. 2019. URL: <a href="http://catalog.odu.edu/courses/"><mark class="gold">catalog.odu.edu/courses/</mark></a>.</li>
					<li>&quot;Academic Tutoring in Comprehensive Universities.&quot; <i>Hanover Research</i>, 2014. URL: <a href="https://www.hanoverresearch.com/wp-content/uploads/2017/08/Academic-Tutoring-in-Comprehensive-Universities.pdf"><mark class="gold">https://www.hanoverresearch.com/wp-content/uploads/2017/08/Academic-Tutoring-in-Comprehensive-Universities.pdf</mark></a>.</li>
				</ol>
			</div><br>
		</div>
		<div class="backgroundBox2">
		<h2>Student Behaviors</h2>
			<hr class="divider green"><br>
			<div class="References">
				<ol>
					<li>Ciscell, Galen, et al. &quot;Barriers to Accessing Tutoring Services Among Students Who Received a MidSemester Warning.&quot; <i>ERIC</i>, Pacific Lutheran University - Department of Sociology, 2016. URL: <a href="http://files.eric.ed.gov/fulltext/EJ1114513.pdf"><mark class="green">files.eric.ed.gov/fulltext/EJ1114513.pdf</mark></a>.</li>
					<li>Evans MDR, Kelley P and Kelley J (2017). Identifying the Best Times for Cognitive Functioning Using New Methods: Matching University Times to Undergraduate Chronotypes. Front. Hum. <i>Neurosci</i>. 11:188. doi: 10.3389/fnhum.2017.00188. URL: <a href="https://www.frontiersin.org/articles/10.3389/fnhum.2017.00188/full?&utm_source=Email_to_authors_&utm_medium=Email&utm_content=T1_11.5e1_author&utm_campaign=Email_publication&field=&journalName=Frontiers_in_Human_Neuroscience&id=239492"><mark class="green">https://www.frontiersin.org/articles/10.3389/fnhum.2017.00188/full?&utm_source=Ema ...</mark></a>.</li>
					<li>Fry, Natalie. &quot;New Research Reveals That College Students Study Best Later in the Day.&quot; <i>NevadaToday</i>, University of Nevada, Reno, 11 Apr. 2017. URL: <a href="http://www.unr.edu/nevada-today/news/2017/best-time-of-day-to-study"><mark class="green">www.unr.edu/nevada-today/news/2017/best-time-of-day-to-study</mark></a>.</li>
					<li>&quot;Peer Assisted Learning&quot; <i>BMC Education</i>, 8 March 2006 URL: <a href="https://bmcmededuc.biomedcentral.com/articles/10.1186/1472-6920-6-18"><mark class="green">https://bmcmededuc.biomedcentral.com/articles/10.1186/1472-6920-6-18</mark></a>.</li>
					<li>Pierce, Dennis. &quot;Supporting Students Beyond Financial Aid&quot;, 2016 URL: <a href="http://eds.b.ebscohost.com.proxy.lib.odu.edu/ehost/detail/detail?vid=0&sid=d93df6c4-3729-4b62-8d58-95e25c309878%40sessionmgr102&bdata=JnNpdGU9ZWhvc3QtbGl2ZSZzY29wZT1zaXRl#AN=114789419&db=ehh"><mark class="green">http://eds.b.ebscohost.com.proxy.lib.odu.edu/ehost/detail/detail?...</mark></a>.</li>
					<li>Qayyum, Adnan. &quot;Student Help-Seeking Attitudes and Behaviors in a Digital Era.&quot; <i>International Journal of Educational Technology in Higher Education</i>, vol. 15, no. 1, 2018, doi:10.1186/s41239-018-0100-7. URL: <a href="https://educationaltechnologyjournal.springeropen.com/articles/10.1186/s41239-018-0100-7"><mark class="green">https://educationaltechnologyjournal.springeropen.com/articles/10.1186/s41239-018-0100-7</mark></a>.</li>
					<li>&quot;Student as Peer Tutors&quot; <i>BMC Education</i>, 9 June. 2014. URL: <a href="https://bmcmededuc.biomedcentral.com/articles/10.1186/1472-6920-14-115"><mark class="green">https://bmcmededuc.biomedcentral.com/articles/10.1186/1472-6920-14-115</mark></a>.</li>
					<li>Topping, J. Keith. &quot;Trends in Peer Learning&quot;, 19 Jan 2007 URL: <a href="https://www.tandfonline.com/doi/full/10.1080/01443410500345172?scroll=top&needAccess=true"><mark class="green">https://www.tandfonline.com/doi/full/10.1080/01443410500345172?scroll=top&needAccess=tru</mark></a>.</li> 
				</ol>
			</div><br>
		</div>
		<div class="backgroundBox2">
		<h2>Competition</h2>
			<hr class="divider blue"><br>
			<div class="References">
				<ol>
					<li>&quot;Facebook - Groups.&quot; <i>Facebook Help Center</i>, Facebook, 2019. URL: <a href="http://www.facebook.com/help/1629740080681586?helpref=hc_global_nav"><mark class="blue">www.facebook.com/help/1629740080681586?helpref=hc_global_nav</mark></a>.</li>
					<li>&quot;Find a Local In-Home Tutor Today.&quot; <i>HeyTutor</i>, HeyTutor LLC. URL: <a href="http://heytutor.com/"><mark class="blue">heytutor.com/</mark></a>.</li>
					<li>&quot;Skooli Tutors Online.&quot; <i>Skooli Online Tutoring</i>, Skooli, Feb. 2019. URL: <a href="http://www.skooli.com/prices/students"><mark class="blue">www.skooli.com/prices/students</mark></a>.</li>
					<li>&quot;Tutor Matching Service - How It Works.&quot; <i>Tutor Matching Service</i>, Tutor Matching Service, 2019. URL: <a href="http://tutormatchingservice.com/#/about"><mark class="blue">tutormatchingservice.com/#/about</mark></a>.</li>
					<li>&quot;Tutors - Care.com.&quot; <i>Care.com</i>, Care.com, Feb. 2019. URL: <a href="http://www.care.com/tutors"><mark class="blue">www.care.com/tutors</mark></a>.</li>
					<li>&quot;Tutor.com - The Princeton Review.&quot; <i>Tutor.com</i>, The Princeton Review, URL: <a href="http://www.tutor.com"><mark class="blue">www.tutor.com</mark></a>.</li>
					<li>&quot;Wyzant.&quot; <i>Wyzant Resources</i>, Wyzant Inc., Feb. 2019. URL: <a href="http://www.wyzant.com/howitworks/students"><mark class="blue">www.wyzant.com/howitworks/students</mark></a>.</li>
				</ol>
			</div><br>
		</div>
		<div class="backgroundBox2">
			<h2>Icon Credits</h2>
			<hr class="divider bluer"><br>
			<div class="References">
				<ol>
					<li>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank"><mark class="bluer">CC 3.0 BY</mark></a></li>
					<li>Icons made by <a href="https://www.flaticon.com/authors/popcorns-arts" title="Icon Pond">Icon Pond</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 	title="Creative Commons BY 3.0" target="_blank"><mark class="bluer">CC 3.0 BY</mark></a></li>
				</ol>
			</div>
		</div>
		<br>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("modules/footer.php");
		?>
	</body>
</html>