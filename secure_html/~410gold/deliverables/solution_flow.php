<!DOCTYPE html>
<html>
	<title>Tutor Dash - Solution Flow</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/deliverables/solution_flow.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox">
			<h1>Solution Flow</h1>
			<hr class="headerUnderline">
		</div>
		<br>
		<h2>How Tutor Dash Affects The Current Processes</h2>
		<div class="backgroundBox">
			<img src="../images/deliverables/solution-complicated.png" class="center">
		</div>
		<br>
		<h2>Tutor Dash's Solution Flow</h2>
		<div class="backgroundBox">
			<img src="../images/deliverables/solution-simple.png" class="center">
		</div>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>