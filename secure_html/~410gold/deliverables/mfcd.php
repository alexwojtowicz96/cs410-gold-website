<!DOCTYPE html>
<html>
	<title>Tutor Dash - MFCD</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/deliverables/mfcd.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox">
			<h1>M.F.C.D.</h1>
			<hr class="headerUnderline">
		</div>
		<br>
		<div class="backgroundBox">
			<br>
			<img src="../images/deliverables/mfcd.png" class="center">
		</div>
		<br>
		<div class="backgroundBox">
			<h1>Prototype M.F.C.D.</h1>
			<hr class="headerUnderline">
		</div>
		<br>
		<div class="backgroundBox">
			<p>For our Prototype we will constrain our development to just Android, and we will limit our user base to ODU students.</p>
			<hr class="headerUnderline">
			<br>
			<img src="../images/deliverables/prototype_mfcd.png" class="center">
		</div>
	</body>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
</html>