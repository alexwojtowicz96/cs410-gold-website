<!DOCTYPE html>
<html>
	<title>Tutor Dash - Risk Matrix</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/deliverables/risks.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox">
			<h1>Risk Matrix</h1>
			<hr class="headerUnderline">
		</div>
		<br>
		<div class="backgroundBox riskMatrixInfo">
			<div class="riskMatrixSec">
				<h3 class="probability">Probability</h3>
				<h3 class="impact">Impact</h3>
				<table class="risk">
					<tr>
						<th> </th>
						<th class="grey">Very Low</th>
						<th class="grey">Low</th>
						<th class="grey">Moderate</th>
						<th class="grey">High</th>
						<th class="grey">Very High</th>
					</tr>
					<tr>
						<td class="grey">Very High</td>
						<td class="yellow">T3, T4, L1</td>
						<td class="brown">T6, L2</td>
						<td class="red"></td>
						<td class="red">C3, C4</td>
						<td class="red"></td>
					</tr>
					<tr>
						<td class="grey">High</td>
						<td class="yellow">T1, C7</td>
						<td class="yellow">C6</td>
						<td class="brown"></td>
						<td class="red"></td>
						<td class="red"></td>
					</tr>
					<tr>
						<td class="grey">Moderate</td>
						<td class="green">C5</td>
						<td class="yellow">T8</td>
						<td class="yellow">C1, C8, T7</td>
						<td class="brown"></td>
						<td class="red"></td>
					</tr>
					<tr>
						<td class="grey">Low</td>
						<td class="green">C2, T5</td>
						<td class="green"></td>
						<td class="yellow">C10, T2</td>
						<td class="yellow"></td>
						<td class="brown"></td>
					</tr>
					<tr>
						<td class="grey">Very Low</td>
						<td class="green"></td>
						<td class="green">C9</td>
						<td class="green"></td>
						<td class="yellow"></td>
						<td class="yellow"></td>
					</tr>
				</table>
			</div>
			<br>
			<table class="tableBreakDown">
				<tr class="TBDrow">
					<th>Customer Risks</th>
					<th>Customer Mitigations</th>
				</tr>
				<tr class="TBDrow">
					<td><mark>(C1)</mark> Student finds the tutor to be unhelpful.</td>
					<td><mark>(C1)</mark> Support of a rating system that indicates this to other customers. Refund money based on circumstances (Business takes a loss).</td>
				</tr>
				<tr class="TBDrow">
					<td><mark>(C2)</mark> Prospective tutors faking their qualifications.</td>
					<td><mark>(C2)</mark> Require students to upload their official transcript in PDF format for qualification analysis. This file needs to contain the university&apos;s digital signature so not just any any PDF transcript will be considered valid.</td>
				</tr>
				<tr class="TBDrow">
					<td><mark>(C3)</mark> Shortage of tutors</td>
					<td><mark>(C3)</mark> Give small bonuses to tutors for limited time to grow tutor market (Business takes a loss).</td>
				</tr>
				<tr class="TBDrow">
					<td><mark>(C4)</mark> Shortage of tutees</td>
					<td><mark>(C4)</mark> Give free sessions to new members, and give loyalty-free sessions for a certain number of usages (Business takes a loss).</td>
				</tr>
				<tr class="TBDrow">
					<td><mark>(C5)</mark> The tutee/tutor leaves a false negative review and/or rating</td>
					<td><mark>(C5)</mark> Withhold ratings and reviews until both users involved agree that the ratings are justified. Give users the ability to challenge ratings/reviews and require explanations for poor ratings/reviews.</td>
				</tr>
				<tr class="TBDrow">
					<td><mark>(C6)</mark> Users misuse the application; use app maliciously.</td>
					<td><mark>(C6)</mark> Create terms of service agreement, and blacklist individuals who violate the service agreement.</td>
				</tr>
				<tr class="TBDrow">
					<td><mark>(C7)</mark> Identity theft. Non-users impersonate users and/or users impersonate other users.</td>
					<td><mark>(C7)</mark> Implement a means authenticating users each time they navigate to a window from outside of the app back into some window inside of the app. This is similar to online banking application methods. Implement a &quot;handshake&quot; agreement where users must confirm their scheduled meeting at the start time.</td>
				</tr>
				<tr class="TBDrow">
					<td><mark>(C8)</mark> Tutor/Tutee doesn't show up to their meeting.</td>
					<td><mark>(C8)</mark> Payment is preallocated. Some deposit is required. Tutors and tutees are both rated, and if one or the other doesn’t show up to the meeting then they will receive poor ratings.</td>
				</tr>
				<tr class="TBDrow">
					<td><mark>(C9)</mark> Users try to book overlapping sessions.</td>
					<td><mark>(C9)</mark> Only allow users to book appointments for times they do not currently have any session scheduled. This applies to both tutees and tutors.</td>
				</tr>
				<tr class="TBDrow">
					<td><mark>(C10)</mark> Tutors are not adequately prepared to engage with tutees via web conferencing.</td>
					<td><mark>(C10)</mark> Alert users of the minimum requirements for web conference meetings upon selecting &apos;web conferencing&apos; as a tutoring preference.</td>
				</tr>
			</table>
			<table class="tableBreakDown">
				<tr>
					<th>Technical Risks</th>
					<th>Technical Mitigations</th>
				</tr>
				<tr>
					<td><mark>(T1)</mark> Transaction failure. Payment is not received.</td>
					<td><mark>(T1)</mark> Integrate usage of a 3rd party API designed to handle e-transactions.</td>
				</tr>
				<tr>
					<td><mark>(T2)</mark> Difficulty automating the process of reading a submitted transcript</td>
					<td><mark>(T2)</mark> Define reusable code for the general case, and optimize as more information is discovered.</td>
				</tr>
				<tr>
					<td><mark>(T3)</mark> Database server failure</td>
					<td><mark>(T3)</mark> Use reliable servers maintained by large corporations (i.e. Google&apos;s Firebase).</td>
				</tr>
				<tr>
					<td><mark>(T4)</mark> Security breach</td>
					<td><mark>(T4)</mark> Define known security features to prevent unauthorized access.</td>
				</tr>
				<tr>
					<td><mark>(T5)</mark> Some android phones not being able to run application.</td>
					<td><mark>(T5)</mark> Define minimum SDK for weaker hardware phones, and define normal SDK for standard phones.</td>
				</tr>
				<tr>
					<td><mark>(T6)</mark> Network server failure.</td>
					<td><mark>(T6)</mark> Server redundancy.</td>
				</tr>
				<tr>
					<td><mark>(T7)</mark> Pay-rate algorithm does not compute competitive rates for tutors.</td>
					<td><mark>(T7)</mark> Determine a base pay that will increase/decrease due to various factors. Compare the pay-rates of similarly rated tutors who tutor the same courses.</td>
				</tr>
				<tr>
					<td><mark>(T8)</mark> Web-conferencing meeting does not get set up properly.</td>
					<td><mark>(T8)</mark> Use Google Hangouts and use a G Suite (which is maintained by Tutor Dash) to act as the host for each web conference between two users (tutor and tutee).</td>
				</tr>
			</table>
			<table class="tableBreakDown">
				<tr>	
					<th>Legal Risks</th>
					<th>Legal Mitigations</th>
				</tr>
				<tr>
					<td><mark>(L1)</mark> Possible violation of The Family Education Rights and Privacy Act (FERPA).</td>
					<td><mark>(L1)</mark> Have students explicitly agree to terms of service where they agree to disclose their grades. Transcripts are discarded after eligibility is determined.</td>
				</tr>
				<tr>
					<td><mark>(L2)</mark> User base uses application for illegal activities.</td>
					<td><mark>(L2)</mark> Define explicitly in terms of service that illegal activities will not be tolerated and any such action will be reported to law enforcement.</td>
				</tr>
			</table>
		</div>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>