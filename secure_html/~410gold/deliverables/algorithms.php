<!DOCTYPE html>
<html>
	<title>Tutor Dash - Algorithms</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/deliverables/algorithms.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox">
			<h1>Algorithms</h1>
			<hr class="headerUnderline">
		</div>
		<br>
		<!-- TRANSCRIPT PARSER -->
		<div class="backgroundBox">
			<h2>PDF Transcript Parser</h2>
			<div class="constrain">
				<ul>
					<li><b>Purposes</b></li>
					<ul>
						<li>Determine which classes a user is qualified to tutor</li>
						<li>Add new courses being tutored to the database</li>
					</ul>
					<br>
					<li><b>Tools</b></li>
					<ul>
						<li>PDFBox Java Library</li>
					</ul>
					<br> 
					<li><b>Parameters</b></li>
					<ul>
						<li>University Name</li>
						<li>Transcript (PDF)</li>
						<li>Minimum Qualifying Grade</li>
					</ul>
				</ul>
			</div>
			<img src="../images/deliverables/algorithms/transcript.png" class="center">
		</div>
		<br>
		<!-- PAY RATE -->
		<div class="backgroundBox">
			<h2>Pay-Rate Calculator</h2>
			<div class="constrain">
				<ul>
					<li><b>Purposes</b></li>
					<ul>
						<li>Keep pay-rates competitive by providing an upper bound based on various factors</li>
						<li>Alleviate the possibility of tutors not getting hired often enough</li>
					</ul>
					<br>
					<li><b>Parameters</b></li>
					<ul>
						<li>Tutor rating (course-specific)</li>
						<li>Tutor rating (overall)</li>
						<li>Course demand/popularity</li>
						<li>Tutor&apos;s experience</li>
						<li>Time of day</li>
						<li>Mean/Standard deviation of rates for courses</li>
						<li>Time since tutor&apos;s last request in that course</li>
					</ul>
				</ul>
			</div>
			<img src="../images/deliverables/algorithms/pay-rate.png" class="center">
		</div>
		<br>
		<!-- RELATIVE DISTANCE -->
		<div class="backgroundBox">
			<h2>Relative Distance Estimator</h2>
			<div class="constrain">
				<ul>
					<li><b>Purposes</b></li>
					<ul>
						<li>Display how far a set of users B is from user A based on user A&apos;s search results</li>
						<li>Keep the distance information updated as often as possible</li>
					</ul>
					<br>
					<li><b>Parameters</b></li>
					<ul>
						<li>Time</li>
						<li>Time interval between updates</li>
						<li>Android device GPS coordinates</li>
						<ul>
							<li>User A&apos;s longitude/latitude</li>
							<li>Every user in B&apos;s longitude/latitude</li>
						</ul>
					</ul>
				</ul>
			</div>
			<img src="../images/deliverables/algorithms/distance.png" class="center">
		</div>
		<br>
		<!-- WEB CONFERENCES -->
		<div class="backgroundBox">
			<h2>Web Conference Appointment Creator</h2>
			<div class="constrain">
				<ul>
					<li><b>Purpose</b></li>
					<ul>
						<li>Create a google hangouts meeting for two user’s if the tutoring session in question is via web-conferencing</li>
					</ul>
					<br>
					<li><b>Tools</b></li>
					<ul>
						<li>Selenium WebDriver (Java)</li>
					</ul>
					<br> 
					<li><b>Parameters</b></li>
					<ul>
						<li>Start time of session</li>
						<li>End time of session</li>
						<li>User email addresses</li>
						<ul>
							<li>Tutor</li>
							<li>Tutee</li>
						</ul>
					</ul>
				</ul>
			</div>
			<img src="../images/deliverables/algorithms/web-conference.png" class="center">
		</div>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>