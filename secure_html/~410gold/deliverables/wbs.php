<!DOCTYPE html>
<html>
	<title>Tutor Dash - Work Breakdown Structure</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/deliverables/wbs.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox">
			<h1>Work Breakdown Structure</h1>
			<hr class="headerUnderline">
		</div>
		<br>
		<div class="backgroundBox">
			<br>
			<img src="../images/deliverables/wbs.png" class="center">
		</div>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>