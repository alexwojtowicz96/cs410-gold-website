<!DOCTYPE html>
<html>
	<title>Tutor Dash - Process Flows</title>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="../images/icons_logos/favicon2.png"/>
		<link rel="stylesheet" type="text/css" href="../css/deliverables/process_flows.css">
		<link rel="stylesheet" type="text/css" href="../css/nav.css">
		<link rel="stylesheet" type="text/css" href="../css/footer.css">
	</head>
	<body>
		<?php
			// fetches contents from navi.php file in modules folder
			echo file_get_contents("../modules/navi.php");
		?>
<!---->
<!---->
<!---->
		<div class="backgroundBox">
			<h1>Current Process Flows</h1>
			<hr class="headerUnderline">
		</div>
		<br>
		<h2>Tutees Seeking Tutors</h2>
		<div class="backgroundBox">
			<img src="../images/deliverables/current-2-seeking-tutors.png" class="center">
		</div>
		<br>
		<h2>Tutors Seeking Tutees</h2>
		<div class="backgroundBox">
			<img src="../images/deliverables/current-1-lack-advertisement.png" class="center">
		</div>
<!-- FOOTER -->
<!-- ...... -->
<!-- ...... -->
		<?php
			echo file_get_contents("../modules/footer.php");
		?>
	</body>
</html>