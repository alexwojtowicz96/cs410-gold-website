"""
You can use this to replace any unique string in every file of our secure_html folder.
This is case sensitive.
It may be useful when we decide to change TutorU to something else.
Don't use this for trivial words like 'is' since 'is' is a substring of 'This':
'This' would be replaced with 'Th*'.
Basically, just use it for the name change...
"""
import os
import re
from glob import glob
# -----------------------------------------------------------------------------


to_replace = ""
with_replace = ""


# -----------------------------------------------------------------------------
def list_files():
    for x in os.walk("."):
        for y in glob(os.path.join(x[0], '*.html')):
            print("    " + y)
# -----------------------------------------------------------------------------


def prompt():
    global to_replace
    global with_replace
    pattern = re.compile(r'\w+')

    while True:
        while True:
            print("What is being replaced?")
            to_replace = input()
            if re.match(pattern, to_replace):
                break
            else:
                print("Error: Invalid input!\n")
        while True:
            print("Replace '" + to_replace + "' with: ")
            with_replace = input()
            if re.match(pattern, with_replace):
                break
            else:
                print("Error: Invalid input!\n")

        print("Warning: This will replace ALL substrings of '" +
              to_replace + "' with '" + with_replace + "' in the files: ")
        list_files()
        print("Are you sure you want to replace '" +
              to_replace + "' with '" + with_replace + "'? (y/n)")
        choice = input()
        if(choice == 'y'):
            break

    print("")
# -----------------------------------------------------------------------------


prompt()
files = []
for x in os.walk("."):
    for y in glob(os.path.join(x[0], '*.html')):
        files.append(y)

for filename in files:
    try:
        replaced_text = "NULL"
        with open(filename, "r") as fin:
            replaced_text = fin.read().replace(to_replace, with_replace)
            fin.close()
        with open(filename, "w") as fin:
            fin.write(replaced_text)
            fin.close()
        print("SUCCESS: " + filename)

    except FileNotFoundError:
        print("ERROR: File could not be modified: " + filename)
